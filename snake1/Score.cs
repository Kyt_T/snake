﻿using System;
using System.Collections.Generic;
using System.Text;

namespace snake1
{
    class Score
    {
        int x;
        int y;
        int score;
        public Score(int _x, int _y)
        {
            x = _x;
            y = _y;
            score = 0;
        }

        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.WriteLine($"Your score is {score}");
        }

        public void Up()
        {
            score++;
        }
    }
}
