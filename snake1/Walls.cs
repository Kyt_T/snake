﻿using System;
using System.Collections.Generic;
using System.Text;

namespace snake1
{
    class Walls
    {
        List<Figure> wallList = new List<Figure>();

        public Walls(int mapWidht, int mapHeight)
        {
            HorisontalLine lineUp = new HorisontalLine(0, mapWidht - 2, 0, '+');
            HorisontalLine lineDown = new HorisontalLine(0, mapWidht - 2, mapHeight - 1, '+');
            VerticalLine lineLeft = new VerticalLine(0, mapHeight - 1, 0, '+');
            VerticalLine lineRight = new VerticalLine(0, mapHeight - 1, mapWidht - 2, '+');

            wallList.Add(lineUp);
            wallList.Add(lineDown);
            wallList.Add(lineLeft);
            wallList.Add(lineRight);
        }

        internal bool IsHit(Figure figure)
        {
            foreach(var wall in wallList)
            {
                if (wall.IsHit(figure))
                    return true;
            }
            return false;
        }

        public void Draw()
        {
            foreach( var wall in wallList)
            {
                wall.DrawFigure();
            }
        }
    }
}
