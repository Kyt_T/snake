﻿using System;
using System.Threading;

namespace snake1
{
    class Program
    {
        static void Main(string[] args)
        {
            //отрисовка рамочки
            Walls walls = new Walls(80, 25);
            walls.Draw();

            //счет
            Score score = new Score(81, 0);
            score.Draw();

            //отрисовка точек
            Point p1 = new Point(4, 5, '*');
            Snake snake = new Snake(p1, 4, Direction.RIGHT);
            snake.DrawFigure();

            FoodCreator foodCreator = new FoodCreator(80, 25, '@');
            Point food = foodCreator.CreateFood();
            food.Draw();

            while(true)
            {
                if(walls.IsHit(snake) || snake.IsHitTail())
                {
                    break;
                }

                if (snake.Eat(food))
                {
                    food = foodCreator.CreateFood();
                    food.Draw();
                    score.Up();
                    score.Draw();
                }
                else
                {
                    snake.Move();
                }

                Thread.Sleep(100);

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey();
                    snake.HandleKey(key.Key);
                }
            }
            //конец игры
            GameOver gameOver = new GameOver(90, 26);
            gameOver.Draw();
        }
    }
}
