﻿using System;
using System.Collections.Generic;
using System.Text;

namespace snake1
{
    class VerticalLine : Figure
    {
        public VerticalLine(int yTopper, int yLower, int x, char sym)
        {
            pList = new List<Point>();
            for (int y = yTopper; y <= yLower; y++)
            {
                Point p = new Point(x, y, sym);
                pList.Add(p);
            }
        }
    }
}
